from django.contrib import admin

from Pay.models import *

admin.site.site_url = None


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('id', 'payment_reference', 'state',
                    'date_created', 'date_updated')
    list_filter = ('state',)


@admin.register(PaymentReference)
class PaymentReferenceAdmin(admin.ModelAdmin):
    list_filter = ('state', 'invoice_id')

    # Si el usuario es de tipo 3 o operador, solo puede editar el campo de value
    # ! Para la prueba se fijo el id del usuario en 3 pero estos registros se deben obtener de la gestion de usuarios o roles
    def get_readonly_fields(self, request, obj=None):
        if request.user.id == 3:
            return ['invoice_id', 'state', 'sponsor_id', 'status_paid', 'description', 'paid', 'code_currency_id', 'date_created', 'date_updated', 'effective_start_date', 'effective_end_date']
        else:
            return super().get_readonly_fields(request, obj)


@admin.register(PaymentReferenceDetail)
class PaymentReferenceDetailAdmin(admin.ModelAdmin):
    list_filter = ('payment_reference', 'service_planning')


@admin.register(PaymentAgreement)
class PaymentAgreementAdmin(admin.ModelAdmin):
    list_filter = ('state',)


@admin.register(PaymentAgreementConcept)
class PaymentAgreementConceptAdmin(admin.ModelAdmin):
    list_filter = ('state', 'concept', 'payment_agreement')


@admin.register(PaymentDetail)
class PaymentDetailAdmin(admin.ModelAdmin):
    list_filter = ('state', 'payment_agreement')


@admin.register(PaymentFrequency)
class PaymentFrequencyAdmin(admin.ModelAdmin):
    list_filter = ('state',)


@admin.register(PlanItem)
class PlanItemAdmin(admin.ModelAdmin):
    list_filter = ('state',)


@admin.register(ServicePlanning)
class ServicePlanningAdmin(admin.ModelAdmin):
    list_filter = ('state',)


@admin.register(ServicePlanItem)
class ServicePlanItemAdmin(admin.ModelAdmin):
    list_filter = ('state', 'service_planning', 'plan_item')


@admin.register(Concept)
class ConceptAdmin(admin.ModelAdmin):
    list_filter = ('state',)
