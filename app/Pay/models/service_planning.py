from django.db import models

from .date_model import DateModel


class ServicePlanning(DateModel):
    state = models.BooleanField(default=True)
    sponsor_id = models.BigIntegerField()
    name = models.CharField(max_length=100)
    strategic_line_id = models.BigIntegerField()
    service_id = models.BigIntegerField()

    def __str__(self):
        return "{} - {}".format(self.id, self.name)

    class Meta:
        verbose_name = 'Service Planning'
        verbose_name_plural = 'Service Plannings'
        db_table = 'pay_service_planning'
        # db_table = '"pay"."service_planning"'
