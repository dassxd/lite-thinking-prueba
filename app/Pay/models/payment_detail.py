from django.db import models
from django.forms import ValidationError

from .date_model import DateModel


class PaymentDetail(DateModel):
    payment_agreement = models.ForeignKey(
        'PaymentAgreement', on_delete=models.PROTECT)
    state = models.BooleanField(default=True)
    value = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return "{} - {}".format(self.id, self.value)
    
    def clean(self):
        if self.payment_agreement.state != True:
            raise ValidationError('Payment agreement is not active.')
        if self.effective_start_date < self.payment_agreement.effective_start_date:
            raise ValidationError(
                'Effective start date must be greater than effective start date of payment agreement.')
        if self.effective_end_date is not None and self.payment_agreement.effective_end_date is not None and self.effective_end_date > self.payment_agreement.effective_end_date:
            raise ValidationError(
                'Effective end date must be less than effective end date of payment agreement.')
        if self.value < 0:
            raise ValidationError(
                'Value must be greater than zero.')
        return super().clean()

    class Meta:
        verbose_name = 'Payment Detail'
        verbose_name_plural = 'Payment Details'
        db_table = 'pay_payment_detail'
        # db_table = '"pay"."payment_detail"'