from django.db import models
from django.forms import ValidationError

from .date_model import DateModel


class PaymentAgreement(DateModel):
    document_type_id = models.BigIntegerField()
    payment_frequency = models.ForeignKey(
        'PaymentFrequency', on_delete=models.PROTECT)
    payment_reference = models.ForeignKey(
        'PaymentReference', on_delete=models.PROTECT)
    state = models.BooleanField(default=True)
    sponsor_id = models.BigIntegerField()

    def __str__(self):
        return "{}".format(self.id)

    def clean(self):
        if self.payment_frequency.state != True:
            raise ValidationError('Payment frequency is not active.')
        if self.effective_start_date < self.payment_frequency.effective_start_date:
            raise ValidationError(
                'Effective start date must be greater than effective start date of payment frequency.')
        if self.effective_end_date is not None and self.payment_frequency.effective_end_date is not None and self.effective_end_date > self.payment_frequency.effective_end_date:
            raise ValidationError(
                'Effective end date must be less than effective end date of payment frequency.')
        if self.payment_reference.state != True:
            raise ValidationError('Payment reference is not active.')
        if self.effective_start_date < self.payment_reference.effective_start_date:
            raise ValidationError(
                'Effective start date must be greater than effective start date of payment reference.')
        if self.effective_end_date is not None and self.payment_reference.effective_end_date is not None and self.effective_end_date > self.payment_reference.effective_end_date:
            raise ValidationError(
                'Effective end date must be less than effective end date of payment reference.')
        return super().clean()

    class Meta:
        verbose_name = 'Payment Agreement'
        verbose_name_plural = 'Payment Agreements'
        db_table = 'pay_payment_agreement'
        # db_table = '"pay"."payment_agreement"'
