from django.db import models
from django.forms import ValidationError

from .date_model import DateModel


class PaymentReferenceDetail(DateModel):
    user_role_id = models.BigIntegerField(unique=True)
    payment_reference = models.ForeignKey(
        'PaymentReference', on_delete=models.PROTECT)
    service_planning = models.ForeignKey(
        'ServicePlanning', on_delete=models.PROTECT)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return "{} - {} - {}".format(self.id, self.payment_reference.value, self.service_planning.name)
    
    def clean(self):
        if self.payment_reference.state != True:
            raise ValidationError('Payment reference is not active.')
        if self.service_planning.state != True:
            raise ValidationError('Service planning is not active.')
        if self.effective_start_date < self.payment_reference.effective_start_date:
            raise ValidationError(
                'Effective start date must be greater than effective start date of payment reference.')
        if self.effective_end_date is not None and self.payment_reference.effective_end_date is not None and self.effective_end_date > self.payment_reference.effective_end_date:
            raise ValidationError(
                'Effective end date must be less than effective end date of payment reference.')
        if self.effective_start_date < self.service_planning.effective_start_date:
            raise ValidationError(
                'Effective start date must be greater than effective start date of service planning.')
        if self.effective_end_date is not None and self.service_planning.effective_end_date is not None and self.effective_end_date > self.service_planning.effective_end_date:
            raise ValidationError(
                'Effective end date must be less than effective end date of service planning.')
        return super().clean()

    class Meta:
        verbose_name = 'Payment Reference Detail'
        verbose_name_plural = 'Payment Reference Details'
        db_table = 'pay_payment_reference_detail'
        # db_table = '"pay"."payment_reference_detail"'
