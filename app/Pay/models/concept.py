from django.db import models

from .date_model import DateModel


class Concept(DateModel):
    name = models.CharField(max_length=100, primary_key=True)
    state = models.BooleanField(default=True)
    type_variable_id = models.BigIntegerField()
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name = 'Concept'
        verbose_name_plural = 'Concepts'
        db_table = 'pay_concept'
        # db_table = '"pay"."concept"'