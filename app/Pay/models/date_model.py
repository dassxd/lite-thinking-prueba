from django.db import models
from django.core.exceptions import ValidationError


class DateModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    effective_start_date = models.DateTimeField()
    effective_end_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True

    def clean(self):
        try:
            if self.effective_end_date is not None and self.effective_end_date < self.effective_start_date:
                raise ValidationError('Effective end date must be greater than effective start date.')
            if self.effective_end_date is not None and self.effective_end_date < self.date_created:
                raise ValidationError('Effective end date must be greater than date created.')
        except ValidationError as e:
            raise e
        except Exception as e:
            pass
        return super().clean()
