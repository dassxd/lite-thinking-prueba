from django.db import models
from django.forms import ValidationError

from .date_model import DateModel


class ServicePlanItem(DateModel):
    state = models.BooleanField(default=True)
    sponsor_id = models.BigIntegerField()
    plan_item = models.ForeignKey(
        'PlanItem', to_field='name', on_delete=models.PROTECT)
    service_planning = models.ForeignKey(
        'ServicePlanning', on_delete=models.PROTECT)
    value = models.DecimalField(max_digits=10, decimal_places=2)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return "{} - {} - {}".format(self.id, self.plan_item.name, self.service_planning.name)

    def clean(self):
        if self.service_planning.state != True:
            raise ValidationError('Service planning is not active.')
        if self.plan_item.state != True:
            raise ValidationError('Plan item is not active.')
        if self.value <= 0:
            raise ValidationError('Value must be greater than zero.')
        if self.effective_start_date < self.service_planning.effective_start_date:
            raise ValidationError(
                'Effective start date must be greater than effective start date of service planning.')
        if self.effective_end_date is not None and self.service_planning.effective_end_date is not None and self.effective_end_date > self.service_planning.effective_end_date:
            raise ValidationError(
                'Effective end date must be less than effective end date of service planning.')
        if self.effective_start_date < self.plan_item.effective_start_date:
            raise ValidationError(
                'Effective start date must be greater than effective start date of plan item.')
        if self.effective_end_date is not None and self.plan_item.effective_end_date is not None and self.effective_end_date > self.plan_item.effective_end_date:
            raise ValidationError(
                'Effective end date must be less than effective end date of plan item.')
        return super().clean()

    class Meta:
        verbose_name = 'Service Plan Item'
        verbose_name_plural = 'Service Plan Items'
        db_table = 'pay_service_plan_item'
        # db_table = '"pay"."service_plan_item"'
