from django.db import models

from .date_model import DateModel


class PaymentReference(DateModel):
    state = models.BooleanField(default=True)
    sponsor_id = models.BigIntegerField()
    invoice_id = models.BigIntegerField(unique=True)
    status_paid = models.BooleanField(default=False)
    description = models.CharField(max_length=255, blank=True, null=True)
    paid = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    value = models.DecimalField(max_digits=10, decimal_places=2)
    code_currency_id = models.BigIntegerField()

    def __str__(self):
        return "{} - {}".format(self.id, self.invoice_id)

    class Meta:
        verbose_name = 'Payment Reference'
        verbose_name_plural = 'Payment References'
        db_table = 'pay_payment_reference'
        # db_table = '"pay"."payment_reference"'
