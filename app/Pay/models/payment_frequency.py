from django.db import models

from .date_model import DateModel


class PaymentFrequency(DateModel):
    state = models.BooleanField(default=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return "{} - {}".format(self.id, self.name)

    class Meta:
        verbose_name = 'Payment Frequency'
        verbose_name_plural = 'Payment Frequencies'
        db_table = 'pay_payment_frequency'
        # db_table = '"pay"."payment_frequency"'