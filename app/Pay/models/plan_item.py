from django.db import models

from .date_model import DateModel


class PlanItem(DateModel):
    name = models.CharField(max_length=100, primary_key=True)
    state = models.BooleanField(default=True)
    sponsor_id = models.BigIntegerField()
    description = models.CharField(max_length=255, blank=True, null=True)
    type_variable_id = models.BigIntegerField()

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name = 'Plan Item'
        verbose_name_plural = 'Plan Items'
        db_table = 'pay_plan_item'
        # db_table = '"pay"."plan_item"'
