from django.db import models
from django.core.exceptions import ValidationError

from .date_model import DateModel


class Payment(DateModel):
    state = models.BooleanField(default=True)
    code = models.CharField(max_length=100)
    user_email = models.EmailField()
    description = models.CharField(max_length=255, blank=True, null=True)
    state_transaction = models.BooleanField(default=False)
    paid = models.DecimalField(max_digits=10, decimal_places=2)
    sponsor_id = models.BigIntegerField()
    payment_reference = models.ForeignKey(
        'PaymentReference', on_delete=models.PROTECT)

    def __str__(self):
        return "{} - {} - {}".format(self.id, self.code, self.payment_reference.invoice_id)

    def clean(self):
        if  self.payment_reference.state != True:
            raise ValidationError('Payment reference is not active.')
        if self.payment_reference.status_paid == True:
            raise ValidationError('Payment reference is already paid.')
        if self.paid <= 0:
            raise ValidationError('Paid must be greater than zero.')
        other_payments = Payment.objects.filter(
            payment_reference=self.payment_reference)
        if other_payments.count() > 0:
            if other_payments.aggregate(models.Sum('paid'))['paid__sum'] + self.paid > self.payment_reference.value:
                raise ValidationError(
                    'Payment is greater than value of payment reference.')
        if self.paid > self.payment_reference.value:
            raise ValidationError(
                'Payment is greater than value of payment reference.')
        if self.effective_start_date < self.payment_reference.effective_start_date:
            raise ValidationError(
                'Effective start date must be greater than effective start date of payment reference.')
        if self.effective_end_date is not None and self.payment_reference.effective_end_date is not None and self.effective_end_date > self.payment_reference.effective_end_date:
            raise ValidationError(
                'Effective end date must be less than effective end date of payment reference.')
        return super().clean()

    class Meta:
        verbose_name = 'Payment'
        verbose_name_plural = 'Payments'
        db_table = 'pay_payment'
        # db_table = '"pay"."payment"'
