from django.db import models
from django.core.exceptions import ValidationError

from .date_model import DateModel


class PaymentAgreementConcept(DateModel):
    state = models.BooleanField(default=True)
    concept = models.ForeignKey(
        'Concept', to_field='name', on_delete=models.PROTECT)
    payment_agreement = models.ForeignKey(
        'PaymentAgreement', on_delete=models.PROTECT)
    value = models.DecimalField(max_digits=10, decimal_places=2)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return "{} - {}".format(self.id, self.value, self.payment_agreement)

    def clean(self):
        if self.concept.state != True:
            raise ValidationError('Concept is not active.')
        if self.payment_agreement.state != True:
            raise ValidationError('Payment agreement is not active.')
        if self.effective_start_date < self.concept.effective_start_date:
            raise ValidationError(
                'Effective start date must be greater than effective start date of concept.')
        if self.effective_end_date is not None and self.concept.effective_end_date is not None and self.effective_end_date > self.concept.effective_end_date:
            raise ValidationError(
                'Effective end date must be less than effective end date of concept.')
        if self.effective_start_date < self.payment_agreement.effective_start_date:
            raise ValidationError(
                'Effective start date must be greater than effective start date of payment agreement.')
        if self.effective_end_date is not None and self.payment_agreement.effective_end_date is not None and self.effective_end_date > self.payment_agreement.effective_end_date:
            raise ValidationError(
                'Effective end date must be less than effective end date of payment agreement.')
        if self.value < 0:
            raise ValidationError(
                'Value must be greater than zero.')
        return super().clean()

    class Meta:
        verbose_name = 'Payment Agreement Concept'
        verbose_name_plural = 'Payment Agreement Concepts'
        db_table = 'pay_payment_agreement_concept'
        # db_table = '"pay"."payment_agreement_concept"'
