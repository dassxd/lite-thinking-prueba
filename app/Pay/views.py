from django.http import JsonResponse
from django.db.models import Sum

from .models import PaymentReference, Payment


def update_paid(request, invoice_id):
    try:
        invoice = PaymentReference.objects.get(invoice_id=invoice_id)
        paids = Payment.objects.filter(
            payment_reference=invoice).values('paid')
        sum_paid = paids.aggregate(Sum('paid'))['paid__sum']
        print(sum_paid)
        invoice.paid = sum_paid
        invoice.save()
        return JsonResponse({'detail': 'Invoice {} updated'.format(invoice_id)}, status=200)
    except PaymentReference.DoesNotExist:
        return JsonResponse({'error': 'Invoice not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)
