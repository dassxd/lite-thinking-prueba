from django.contrib import admin
from django.urls import path

from Pay.views import update_paid

urlpatterns = [
    path('admin/', admin.site.urls),
    path('update_paid/<int:invoice_id>/', update_paid, name='update_paid'),
]
