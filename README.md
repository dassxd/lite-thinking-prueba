**Camilo Alfonso Bermúdez Gil**

# Modelamiento en Django (Esquema de pago)

- Modelos en Django para el diagrama E-R al final (los campos en rojos no se crearon).
- Administrador de modelos a partir de Django Admin. 
- Filtros de búsqueda eficaz para cada modelo.
- Permite la selección, modificación, eliminación e inserción de registros para los diferentes modelos.
- Usuario administrador con permisos totales para todos los modelos.
- Usuario consulta para permisos de solo consulta y modificación para todos los modelos.
- Usuario operador con permisos de visualización solo para el modelo payment_reference y modificación solo para el campo value.
- Aplica validaciones para los principales modelos.
- Servicio que sume los valores paid del modelo payment para un invoice_id especifico, y este resultado debe aparecer en el campo paid del modelo payment_reference.


## Instrucciones de despliegue 🚀

Para desplegar el proyecto se debe tener python superior a la versión 3.8 y ejecutar los siguientes comandos:

```bash
>> python -m venv .venv
>> source .venv/Scripts/activate
>> pip install -r requirements.txt
>> python app/manage.py migrate
>> python app/manage.py runserver
```

## Usuarios de prueba

Para ingresar al administrador de Django se debe ingresar a la siguiente url:

```bash
http://localhost:8000/admin/
```

Y se debe ingresar con los siguientes usuarios:
- **administrador**
    - *Usuario*: admin
    - *Contraseña*: 2023test

- **consulta**
    - *Usuario*: consulta
    - *Contraseña*: 2023test

- **operador**
    - *Usuario*: operador
    - *Contraseña*: 2023test

## Servicio para actualizar el valor pagado de la referencia de pago

Para actualizar el valor pagado de la referencia de pago se debe hacer una petición GET a la siguiente url:

```bash
http://localhost:8000/update_paid/<invoice_id>/
```

Donde el parámetro invoice_id es el id de la referencia de pago que se desea actualizar.

## Diagrama E-R

<img src="./diagrama.png" alt="Mode lo de base de datos" width="500"/>

<img src="./constraints.png" alt="Modelo de base de datos" width="200"/>